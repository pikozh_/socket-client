package socket;

import javafx.scene.control.TableView;
import model.Number;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SocketChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.EnumSet;

public class Socket {

    private SocketChannel channel;
    private SocketAddress socketAddress;
    private ByteBuffer byteBuffer;

    private static final String HOST = "localhost";
    private static final int PORT = 9000;

    public void setCommandStart() throws IOException {

        channel = SocketChannel.open();
        socketAddress = new InetSocketAddress(HOST, PORT);
        if (!channel.isConnected()) {
            channel.connect(socketAddress);
            System.out.println("kuku");
        }
        String result = "Hello";
        byteBuffer = ByteBuffer.allocate(1024);
        byteBuffer.put(result.getBytes());
        byteBuffer.flip();

        while (byteBuffer.hasRemaining()) {
            channel.write(byteBuffer);
        }
        byteBuffer.clear();
    }


    public void getNumber(TableView<Number> table) throws IOException {
        ByteBuffer bytes = ByteBuffer.allocate(1024);
        String response = "";
        while (channel.read(bytes) > 0) {
            response = new String(bytes.array(), "UTF-8");
            table.getItems().add(new Number(response));
            System.out.println(response);
            bytes.clear();
        }
    }

    public void setCommandGet(String path) throws IOException {
        channel = SocketChannel.open();
        socketAddress = new InetSocketAddress("localhost", 9000);

        if (!channel.isConnected()) {
            channel.connect(socketAddress);
            System.out.println("get");
        }

        String result = "Get";
        byteBuffer = ByteBuffer.allocate(1024);
        byteBuffer.clear();
        byteBuffer.put(result.getBytes());
        byteBuffer.flip();
        while (byteBuffer.hasRemaining()) {
            channel.write(byteBuffer);
        }

        Path pathFile = Paths.get(path);
        FileChannel fileChannel = FileChannel.open(pathFile,
                EnumSet.of(StandardOpenOption.CREATE,
                        StandardOpenOption.TRUNCATE_EXISTING,
                        StandardOpenOption.WRITE));
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        while(channel.read(buffer) > 0) {
            buffer.flip();
            fileChannel.write(buffer);
            buffer.clear();
        }
        fileChannel.close();
        System.out.println("Received");
    }
}


