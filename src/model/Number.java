package model;

public class Number {
    private String value;

    public Number(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
